package uz.azn.musicplayer.model

data class Music(val image:Int, val name:String, val authorName:String , val uri:String) {
}