package uz.azn.musicplayer

import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import uz.azn.musicplayer.databinding.ActivityMainBinding
import uz.azn.musicplayer.music.MusicFragment
import uz.azn.musicplayer.musicPlayer.MusicPlayerFragment

class MainActivity : AppCompatActivity() {
    private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        if (ContextCompat.checkSelfPermission(
                this,
                android.Manifest.permission.READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE),1)


        }
        else{
            supportFragmentManager.beginTransaction().replace(binding.frame.id,
               MusicFragment()
            ).commit()

        }
    }
}