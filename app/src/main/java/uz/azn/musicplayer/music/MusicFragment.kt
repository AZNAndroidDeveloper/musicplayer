package uz.azn.musicplayer.music

import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import uz.azn.musicplayer.R
import uz.azn.musicplayer.adapter.MusicAdapter
import uz.azn.musicplayer.databinding.FragmentMusicBinding
import uz.azn.musicplayer.model.Music
import uz.azn.musicplayer.musicPlayer.MusicPlayerFragment
import java.io.File

@Suppress("DEPRECATION")
class MusicFragment : Fragment(R.layout.fragment_music) {
    private val musicAdapter by lazy {
        MusicAdapter(onItemClick = {
            val bundle  = Bundle()
            bundle.putString("uri",it)
            val musicplayer  =MusicPlayerFragment()
            musicplayer.arguments = bundle
            fragmentManager!!.beginTransaction().replace(R.id.frame,musicplayer).commit()

        })
    }
    private val musicList = mutableListOf<Music>()
    private val fileList = mutableListOf<File>()
    private lateinit var binding: FragmentMusicBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentMusicBinding.bind(view)
        getMusic(Environment.getExternalStorageDirectory())
        Log.d("TAG", "onViewCreated: $fileList")


    }

    private fun getMusic(dir: File) {
        Log.d("DirFile", "getFile: ${dir.absolutePath}")
        val listFileOf = dir.listFiles()
        if (listFileOf != null && listFileOf.isNotEmpty()) {
            for (i in listFileOf.indices) {
                if (listFileOf[i].isDirectory) {
                    getMusic(listFileOf[i])
                } else {
                    if (listFileOf[i].name.endsWith(".mp3") || listFileOf[i].name.endsWith(".MP3")) {

                        musicList.add(
                            Music(
                                R.drawable.music_icon,
                                listFileOf[i].name,
                                listFileOf[i].canonicalPath,
                                listFileOf[i].toURI().toString()
                            )
                        )
                        Log.d("User", "getFile: ${listFileOf[i].absolutePath}")
                    }
                }

            }
            with(binding) {
                recycler.apply {
                    layoutManager = LinearLayoutManager(requireContext())
                    adapter = musicAdapter
                }
                musicAdapter.setData(musicList)

            }
        }
    }
}