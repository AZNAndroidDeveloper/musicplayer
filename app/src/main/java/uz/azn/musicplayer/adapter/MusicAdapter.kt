package uz.azn.musicplayer.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import uz.azn.musicplayer.model.Music
import uz.azn.musicplayer.databinding.ViewHolderMusicItemBinding as MusicBinding

class MusicAdapter(
    private val onItemClick: (String) -> Unit
) : RecyclerView.Adapter<MusicAdapter.MusicVIewHolder>() {

   private val elements = mutableListOf<Music>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MusicVIewHolder {
        return MusicVIewHolder(
            MusicBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = elements.size

    override fun onBindViewHolder(holder: MusicVIewHolder, position: Int) {
        holder.onBind(elements[position])
    }

    inner class MusicVIewHolder(private val binding: MusicBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind(element: Music) {
            with(binding) {
                tvMusicName.text = element.name
                tvMusicAuthor.text = element.authorName
                imageMusic.setImageResource(element.image)
                constraint.setOnClickListener {
                    onItemClick.invoke(element.uri)
                }
            }
        }


    }

    fun setData(element: List<Music>) {
        elements.apply { clear(); addAll(element) }
        notifyDataSetChanged()
    }
}