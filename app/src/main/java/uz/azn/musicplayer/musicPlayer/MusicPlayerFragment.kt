package uz.azn.musicplayer.musicPlayer

import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.SeekBar
import androidx.core.net.toUri
import androidx.fragment.app.Fragment
import uz.azn.musicplayer.R
import uz.azn.musicplayer.R.anim.image_anim
import uz.azn.musicplayer.databinding.FragmentMusicPlayerBinding

class MusicPlayerFragment : Fragment(R.layout.fragment_music_player) {
    private lateinit var binding: FragmentMusicPlayerBinding
    private lateinit var song: MediaPlayer
    private var totalDuration = 0
    private var newMax = 0
    private var newPosition = 0
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentMusicPlayerBinding.bind(view)
        val uri = arguments?.getString("uri")
        Log.d("music", "onViewCreated: $uri")

        song = MediaPlayer.create(requireContext(), uri?.toUri())
        song.setVolume(1f, 1f)
        totalDuration = song.duration
        with(binding) {
            timeDuration.text = timeFormatter(song.duration)
            seekBarVoice.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(
                    seekBar: SeekBar?,
                    progress: Int,
                    fromUser: Boolean
                ) {
                    val volume = progress / 100f
                    song.setVolume(volume, volume)

                }

                override fun onStartTrackingTouch(seekBar: SeekBar?) {
                }

                override fun onStopTrackingTouch(seekBar: SeekBar?) {
                }

            })
            val animation  =AnimationUtils.loadAnimation(requireContext(), R.anim.image_anim)

            btnPlay.apply {
                setOnClickListener {
                    if (song.isPlaying) {
                       imageMusic.clearAnimation()
                        song.pause()

                        setBackgroundResource(R.drawable.ic_play_circle)
                    } else {
                        imageMusic.startAnimation(animation)
                        song.start()
                        updatetimer()
                        setBackgroundResource(R.drawable.ic_pause_circle)
                    }
                }
            }
            musicSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(
                    seekBar: SeekBar?,
                    progress: Int,
                    fromUser: Boolean
                ) {
                    // musiqa vaqt ozgartirsak o'zgaradi
                    song.seekTo(progress)

                }

                override fun onStartTrackingTouch(seekBar: SeekBar?) {
                }

                override fun onStopTrackingTouch(seekBar: SeekBar?) {
                }

            }
            )
            musicSeekBar.max = totalDuration


        }

    }

     private fun updatetimer() {
        val thread = Thread(Runnable {
            try {
                Thread.sleep(50)
                while (song.isPlaying) {
                    Thread.sleep(950)
                    with(binding) {
                       activity!!.runOnUiThread {
                            try {
                                newPosition = song.currentPosition
                                newMax = song.duration-newPosition
                                timeOne.text = timeFormatter(newPosition)
                                timeDuration.text = "-${timeFormatter(newMax)}"

                            } catch (ex: Exception) {
                                ex.printStackTrace()
                            }
                           musicSeekBar.max = newMax
                            musicSeekBar.progress = newPosition
                        }
                    }

                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        })
        thread.start()

    }

    fun timeFormatter(time: Int): String {
        var timeFormatResult = ""
        val min = time / 1000 / 60
        var sec = time / 1000 % 60

        timeFormatResult = "$min"
        if (min.toString().length == 1) {
            timeFormatResult = "0$min:"
        } else {
            timeFormatResult = "$min:"
        }

        if (sec < 10) {
            timeFormatResult += "0"
        }
        timeFormatResult += sec
        return timeFormatResult
    }


}